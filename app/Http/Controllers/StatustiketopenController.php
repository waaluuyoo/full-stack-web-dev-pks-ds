<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tiket;
use App\Customer;

class StatustiketopenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tiket = Tiket::select(

            "tiket.id", 
            "tiket.created_at",
            "tiket.keluhan",
            "tiket.status",
            "customer.nm_customer"
        )
        ->leftJoin("customer", "tiket.customer_id", "=", "customer.id")
        ->get();
        //
        //$hub = Hub::all();

        return view('statustiketopen.index',compact('tiket'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tiket = Tiket::find($id);
        $customer = Customer::pluck('nm_customer', 'id');
        //dd($cast);
        return view('statustiketopen.edit',compact('tiket','customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $tiket = Tiket::find($id);
        $tiket->status = $request['status'];
        
        $tiket->save();
        return redirect ('/status_tiket_open');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
