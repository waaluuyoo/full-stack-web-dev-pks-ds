<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hub;
use App\Area;

class HubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $hub = Hub::select(

            "hub.id", 
            "hub.nm_hub",
            "hub.jenis_koneksi",
            "hub.alamat",
            "area.nm_area"
        )
        ->leftJoin("area", "hub.area_id", "=", "area.id")
        ->get();
        //
        //$hub = Hub::all();

        return view('hub.index',compact('hub'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $area = Area::pluck('nm_area', 'id');

        return view('hub.create',compact('area'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
            'nm_hub' => 'required|max:255',
            'jenis_koneksi' => 'required|max:255',
            'area_id' => 'required',
            'alamat' => 'required|max:255',
            ],
            [
            'nm_hub.required' => 'Nama area belum diisi',
            'jenis_koneksi.required' => 'Jenis Koneksi belum diisi',
            'area_id.required' => 'Pilih Area',
            'alamat.required' => 'Alamat belum diisi',
            ]
        );

        $hub = new Hub;
 
        $hub->nm_hub = $request->nm_hub;
        $hub->jenis_koneksi = $request->jenis_koneksi;
        $hub->area_id = $request->area_id;
        $hub->alamat = $request->alamat;
 
        $hub->save();

        return redirect ('/hub');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hub = Hub::find($id);
        $area = Area::pluck('nm_area', 'id');

        //dd($area);
        return view('hub.show',compact('area','hub'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $hub = Hub::find($id);
        $area = Area::pluck('nm_area', 'id');
        //dd($cast);
        return view('hub.edit',compact('area','hub'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate(
            [
            'nm_hub' => 'required|max:255',
            'jenis_koneksi' => 'required|max:255',
            'area_id' => 'required',
            'alamat' => 'required|max:255',
            ],
            [
            'nm_hub.required' => 'Nama area belum diisi',
            'jenis_koneksi.required' => 'Jenis Koneksi belum diisi',
            'area_id.required' => 'Pilih Area',
            'alamat.required' => 'Alamat belum diisi',
            ]
        );

        $hub = Hub::find($id);
 
        $hub->nm_hub = $request['nm_hub'];
        $hub->jenis_koneksi = $request['jenis_koneksi'];
        $hub->area_id = $request['area_id'];
        $hub->alamat = $request['alamat'];
        
        $hub->save();

        return redirect ('/hub');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $hub = Hub::find($id);
 
        $hub->delete();

        return redirect ('/hub');
    }
}
