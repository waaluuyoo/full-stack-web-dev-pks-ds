<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;
use RealRashid\SweetAlert\Facades\Alert;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $area = Area::all();

        return view('area.index',compact('area'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('area.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
            'nm_area' => 'required|max:255',
            ],
            [
            'nm_area.required' => 'Nama area belum diisi',
            ]
        );

        $area = new Area;
 
        $area->nm_area = $request->nm_area;
 
        $area->save();

        Alert::success('Tambah', 'Data berhasil disimpan');
        return redirect ('/area');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $area = Area::find($id);

        //dd($cast);
        return view('area.show',compact('area'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $area = Area::find($id);

        //dd($cast);
        return view('area.edit',compact('area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate(
            [
            'nm_area' => 'required|max:255',
            ],
            [
            'nm_area.required' => 'Nama area belum diisi',
            ]
        );

        $area = Area::find($id);
 
        $area->nm_area = $request['nm_area'];
        
        $area->save();

        return redirect ('/area');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $area = Area::find($id);
 
        $area->delete();

        return redirect ('/area');
    }
}
