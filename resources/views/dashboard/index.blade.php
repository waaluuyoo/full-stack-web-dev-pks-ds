@extends('layout.master')
@section('title')
@endsection
@section('content')

<div class="row row-cols-1 row-cols-lg-2 row-cols-xl-4">
    <div class="col">
        <div class="card radius-10 overflow-hidden">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <p class="mb-0">Total Tiket</p>
                        <h5 class="mb-0">2</h5>
                    </div>
                    <div class="ms-auto">	<i class='bx bx-cart font-30'></i>
                    </div>
                </div>
            </div>
            <div class="" id="chart1"></div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10 overflow-hidden">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <p class="mb-0">Total Tiket Open</p>
                        <h5 class="mb-0">2</h5>
                    </div>
                    <div class="ms-auto">	<i class='bx bx-wallet font-30'></i>
                    </div>
                </div>
            </div>
            <div class="" id="chart2"></div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10 overflow-hidden">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <p class="mb-0">Total Tiket Close</p>
                        <h5 class="mb-0">1</h5>
                    </div>
                    <div class="ms-auto">	<i class='bx bx-group font-30'></i>
                    </div>
                </div>
            </div>
            <div class="" id="chart3"></div>
        </div>
    </div>
</div><!--end row-->
@endsection