@extends('layout.master')
@section('title')
@endsection
@section('content')

<form method="post" action='/hub'>
    @csrf
    <div class="row">
        <div class="col-xl-12 mx-auto">
            <div class="card border-top border-0 border-4 border-info">
                <div class="card-body">
                    <div class="border p-4 rounded">
                        <div class="card-title d-flex align-items-center">
                            <div><i class="bx bxs-user me-1 font-22 text-info"></i>
                            </div>
                            <h5 class="mb-0 text-info">Hub</h5>
                        </div>
                        <hr>
                        <div class="row mb-3">
                            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Nama Hub</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name='nm_hub' value='{{$hub->nm_hub}}' readonly>
                                @error('nm_hub')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Jenis Koneksi</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name='jenis_koneksi' value='{{$hub->jenis_koneksi}}' readonly>
                                @error('jenis_koneksi')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Area</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="area_id" disabled>
                                    <option value=''>Pilih Area</option>
                                    @foreach ($area as $key => $value)
                                        <option value="{{ $key }}" {{ ( $key == $hub->area_id) ? "selected" : '' }}>                           
                                            {{ $value }} 
                                        </option>
                                    @endforeach    
                                </select>

                                @error('area_id')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Alamat</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="alamat" name='alamat' rows="3" readonly>{{ $hub->alamat }}</textarea>
                                @error('alamat')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <a href='/hub'><button type="button" class="btn btn-info btn-sm">Kembali</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </form>

@endsection